export default {
  incrementScore: ({ commit }, delay) => {
    setTimeout(() => {
      commit('increment', 3)
    }, delay)
  }
}
