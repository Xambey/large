import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  score: 0
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
